Source: python-pyreadr
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-setuptools,
               python3-all-dev,
               python3-pandas,
               python3-pandas-lib,
               cython3,
               librdata-dev,
               zlib1g-dev,
               libbz2-dev,
               liblzma-dev
Standards-Version: 4.6.0
Homepage: https://github.com/ofajardo/pyreadr
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pyreadr
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pyreadr.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-pyreadr
Architecture: any
Depends: ${python3:Depends},
         ${misc:Depends},
         ${shlibs:Depends},
         python3-pandas
Description: read/write R data files via Python pandas
 A python package to read and write R RData and Rds files into/from pandas
 dataframes. It does not need to have R or other external dependencies
 installed.
 .
 It can read mainly R data frames and tibbles. Also supports vectors,
 matrices, arrays and tables. R lists and R S4 objects (such as those
 from Bioconductor) are not supported. Please read the Known limitations
 section and the section on what objects can be read for more information.
 .
 This package is based on the librdata C library by Evan Miller and a
 modified version of the cython wrapper around librdata jamovi-readstat
 by the Jamovi team.
